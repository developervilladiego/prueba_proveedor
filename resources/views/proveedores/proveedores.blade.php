<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


        <title>Registro Proveedor</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <div class="container">
    <h2>Proveedores Registrados</h2>
    <div class="form-group">

    <a href="/"><button type="button"   class="btn btn-Primary">Inicio</button></a>
    <div>
    <table class="table table-responsive " style="height: 350px; overflow-y: auto">
        <thead>
        <tr>
            <th>Nombres</th>
            <th>Nit</th>
            <th>Tipo Empresa</th>
            <th>Empresa</th>
            <th>Contacto</th>
            <th>Telefono Empresa</th>
        </tr>
        </thead>
        @foreach($proveedores as $provee)
        <tr>
            <td>{{$provee->nombre}}</td>        
            <td>{{$provee->nit}}</td>        
            @if($provee->tipo_empresa==1)
                <td><a>Persona Natural</a></td>        
            @elseif($provee->tipo_empresa==2)
                <td><a>Persona Juridica</a></td>
            @else  
            <td><a>Consorcio</a></td>
            @endif            
            @if($provee->empresa==1)
                <td><a>Publica</a></td>        
            @else  
            <td><a>Privada</a></td>
            @endif                    
            <td>{{$provee->contacto}}</td>  
            <td>{{$provee->telefono_contacto}}</td>        
                  
                    
        </tr>
        @endforeach
    </table>
    </div>
  
    </body>
</html>
