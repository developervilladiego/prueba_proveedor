<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Registro Proveedor</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <div class="container">
    <form action="registro" method="POST" >
    @if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif
<div class="container">
<h2>Registro proveedor</h2>
<div>
        <div class="form-group">
            <label for="full_name_id" class="control-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required >
        </div>    
    
        <div class="form-group"> <!-- Street 1 -->
            <label for="street1_id" class="control-label">Rut/Nit</label>
            <input type="number"  max="9999999999" class="form-control" id="rut_nit" name="rut_nit" placeholder="Rut/Nit" required >
        </div>                                                      
                                
        <div class="form-group"> <!-- State Button -->
            <label for="state_id" class="control-label">Tipo Empresa</label>
            <select class="form-control" name="tipo_empresa" id="tipo_empresa" required >
            @foreach($tipo_empresas as $tipos)
                <option value="{{$tipos->id}}">{{$tipos->tipo_empresa}}</option>
                @endforeach
            </select>                    
        </div>
        <div class="form-check form-check-inline">
        <label for="state_id" class="control-label">Empresa : </label>        
            <label class="form-check-label">
             <input class="form-check-input" type="radio" name="tipo" id="tipo" value="1" required >Privada
             </label>
        </div>
        <div class="form-check form-check-inline">
        <label class="form-check-label">
            <input class="form-check-input" type="radio" name="tipo" id="tipo" value="2" required >publica
        </label>
        </div>
        <div class="form-group"> <!-- Zip Code-->
            <label for="zip_id" class="control-label">Contacto</label>
            <input type="text" class="form-control" id="contacto" name="contacto"  required >
        </div>        
        <div class="form-group"> <!-- Zip Code-->
            <label for="zip_id" class="control-label">Telefono contacto</label>
            <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Numero Telefonico" required >
        </div>       
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Registar Proveedor</button>
           
            <a href="/"><button type="button"   class="btn btn-danger">Cancelar</button></a>
        </div>     
    </form>
    </div>
    </body>
</html>
