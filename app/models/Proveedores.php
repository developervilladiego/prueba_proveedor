<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $table= "proveedores";
    public $timestamp = true;
}
