<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    protected $table= "tipo_empresa";
    public $timestamp = true;
}
