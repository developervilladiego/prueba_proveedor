<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Empresas;
use App\models\Proveedores;



class ProveedoresController extends Controller
{
     public function vistaRegistro(){
          $tipo_empresas = Empresas::all();

          return \View::make('proveedores/registro_proveedor',compact('tipo_empresas'));

     }
    public function registroProveedor(Request $request){
        $result = Proveedores::where("nit","=",$request->rut_nit)->get();
        if(count($result)){
            return redirect()->back()->with('alert', 'Nit ya registrado');
        }
        $proveedor = new  Proveedores();
        $proveedor->nombre = $request->nombre;
        $proveedor->nit = $request->rut_nit;
        $proveedor->tipo_empresa=$request->tipo_empresa;
        $proveedor->empresa=$request->tipo;
        $proveedor->contacto=$request->contacto;
        $proveedor->telefono_contacto=$request->telefono;

        $proveedor->save();
        return view('welcome')->with('alert', 'Proveedor registrado');;

    }

    public function provedoresR(){
            $proveedores = Proveedores::all();

            return \View::make('proveedores/proveedores',compact('proveedores'));

    }
}
